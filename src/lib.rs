pub mod messages;

use std::{
    collections::{HashMap, HashSet},
    sync::mpsc::Sender,
};

use crate::messages::GilrsRebindGamepad;
use gilrs::{EventType, GamepadId, Gilrs};
use waef_controls::system::gamepad::{
    GamepadAxis, GamepadAxisMoved, GamepadButton, GamepadButtonEvent, GamepadButtonState,
    GamepadConnected, GamepadDisonnected, GamepadTrigger, GamepadTriggerMoved,
};
use waef_core::{
    actors::{ActorAlignment, ActorsPlugin, HasActorAlignment, IsActor},
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsDisposable, IsMessage, Message},
    timers::messages::OnAppTick,
};

struct GamepadInfo {
    gamepad_index: u32,
    axis: HashMap<u32, (f32, f32)>,
}

pub struct GilrsActor {
    dispatch: Sender<Message>,
    gilrs: Gilrs,
    connected_gamepads: HashMap<GamepadId, GamepadInfo>,
    next_gamepad_index: u32,
    recycled_gamepad_indexes: Vec<u32>,
}

#[cfg(target_arch = "wasm32")]
unsafe impl Send for GilrsActor {}

impl GilrsActor {
    pub fn new(dispatch: Sender<Message>) -> Self {
        tracing::info!("Initializing Gilrs for Gamepad support");

        let gilrs = Gilrs::new().expect("Could not initialize Gilrs");

        let mut connected_gamepads = HashMap::new();
        let mut next_gamepad_index = 1;

        let already_connected_gamepads = gilrs.gamepads().collect::<Vec<_>>();
        if !already_connected_gamepads.is_empty() {
            tracing::info!(
                "Gilrs found {} already connected gamepads",
                already_connected_gamepads.len()
            );

            for (gamepad_id, _) in already_connected_gamepads {
                _ = connected_gamepads.insert(
                    gamepad_id,
                    GamepadInfo {
                        axis: HashMap::new(),
                        gamepad_index: next_gamepad_index,
                    },
                );
                _ = dispatch.send(
                    GamepadConnected {
                        gamepad_index: next_gamepad_index,
                    }
                    .into_message(),
                );
                next_gamepad_index = next_gamepad_index + 1;
            }
        }

        Self {
            dispatch,
            gilrs,
            connected_gamepads,
            next_gamepad_index,
            recycled_gamepad_indexes: Vec::new(),
        }
    }

    fn gilrs_to_button_code(btn: gilrs::Button) -> GamepadButton {
        match btn {
            gilrs::Button::South => GamepadButton::South,
            gilrs::Button::East => GamepadButton::East,
            gilrs::Button::North => GamepadButton::North,
            gilrs::Button::West => GamepadButton::West,
            gilrs::Button::C => GamepadButton::C,
            gilrs::Button::Z => GamepadButton::Z,
            gilrs::Button::LeftTrigger => GamepadButton::ShoulderLeft,
            gilrs::Button::LeftTrigger2 => GamepadButton::ShoulderLeftAlt,
            gilrs::Button::RightTrigger => GamepadButton::ShoulderRight,
            gilrs::Button::RightTrigger2 => GamepadButton::ShoulderRightAlt,
            gilrs::Button::Select => GamepadButton::Select,
            gilrs::Button::Start => GamepadButton::Start,
            gilrs::Button::Mode => GamepadButton::Mode,
            gilrs::Button::LeftThumb => GamepadButton::StickLeft,
            gilrs::Button::RightThumb => GamepadButton::StickRight,
            gilrs::Button::DPadUp => GamepadButton::DPadUp,
            gilrs::Button::DPadDown => GamepadButton::DPadDown,
            gilrs::Button::DPadLeft => GamepadButton::DPadLeft,
            gilrs::Button::DPadRight => GamepadButton::DPadRight,
            gilrs::Button::Unknown => GamepadButton::Unknown,
        }
    }

    fn get_gamepad_index(&self, id: GamepadId) -> Option<u32> {
        self.connected_gamepads.get(&id).map(|gp| gp.gamepad_index)
    }

    fn apply_axis(
        &mut self,
        gamepad: GamepadId,
        axis: GamepadAxis,
        x: Option<f32>,
        y: Option<f32>,
    ) -> Option<(u32, (f32, f32))> {
        if let Some(gamepad) = self.connected_gamepads.get_mut(&gamepad) {
            if let Some(value) = gamepad.axis.get_mut(&(axis as u32)) {
                value.0 = x.unwrap_or(value.0);
                value.1 = y.unwrap_or(value.1);
                Some((gamepad.gamepad_index, (value.0, value.1)))
            } else {
                let x = x.unwrap_or(0.0);
                let y = y.unwrap_or(0.0);
                _ = gamepad.axis.insert(axis as u32, (x, y));
                Some((gamepad.gamepad_index, (x, y)))
            }
        } else {
            None
        }
    }
}
impl HasActorAlignment for GilrsActor {
    fn alignment(&self) -> ActorAlignment {
        ActorAlignment::Processing
    }
}
impl IsDispatcher for GilrsActor {
    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf(HashSet::from([
            OnAppTick::category_name().into(),
            GilrsRebindGamepad::category_name().into(),
        ]))
    }

    fn dispatch(&mut self, msg: &Message) -> ExecutionResult {
        if msg.name == GilrsRebindGamepad::category_name() {
            if let Some(eve) = GilrsRebindGamepad::from_message(msg) {
                let mut current_id = None;
                let mut target_id = None;
                for (gamepad, info) in self.connected_gamepads.iter() {
                    if info.gamepad_index == eve.current_index {
                        current_id = Some(*gamepad);
                    } else if info.gamepad_index == eve.target_index {
                        target_id = Some(*gamepad);
                    }
                }
                match (current_id, target_id) {
                    (Some(current_id), Some(target_id)) => {
                        self.connected_gamepads
                            .get_mut(&current_id)
                            .unwrap()
                            .gamepad_index = eve.target_index;
                        self.connected_gamepads
                            .get_mut(&target_id)
                            .unwrap()
                            .gamepad_index = eve.current_index;
                    }
                    (Some(_), None) => {
                        tracing::warn!("Request was made to rebind gamepad {} to {} but no gamepad with target index was connected", eve.current_index, eve.target_index);
                    }
                    (None, Some(_)) => {
                        tracing::warn!("Request was made to rebind gamepad {} to {} but no gamepad with that index was connected", eve.current_index, eve.target_index);
                    }
                    (None, None) => {
                        tracing::warn!("Request was made to rebind gamepad {} to {} but no gamepad with either index was connected", eve.current_index, eve.target_index);
                    }
                }
            }
        } else {
            while let Some(eve) = self.gilrs.next_event() {
                let _guard = tracing::trace_span!("gilrs:process_event");
                match eve.event {
                    EventType::ButtonPressed(btn, _) => {
                        if let Some(gamepad_index) = self.get_gamepad_index(eve.id) {
                            _ = self.dispatch.send(
                                GamepadButtonEvent {
                                    button: Self::gilrs_to_button_code(btn),
                                    gamepad_index,
                                    state: GamepadButtonState::Pressed,
                                }
                                .into_message(),
                            )
                        }
                    }
                    EventType::ButtonRepeated(_, _) => {}
                    EventType::ButtonChanged(btn, degree, _) => {
                        if let Some(gamepad_index) = self.get_gamepad_index(eve.id) {
                            if let Some(trigger) = match btn {
                                gilrs::Button::LeftTrigger => Some(GamepadTrigger::Left),
                                gilrs::Button::LeftTrigger2 => Some(GamepadTrigger::LeftAlt),
                                gilrs::Button::RightTrigger => Some(GamepadTrigger::Right),
                                gilrs::Button::RightTrigger2 => Some(GamepadTrigger::RightAlt),
                                _ => None,
                            } {
                                _ = self.dispatch.send(
                                    GamepadTriggerMoved {
                                        trigger,
                                        gamepad_index,
                                        value: degree,
                                    }
                                    .into_message(),
                                )
                            }
                        }
                    }
                    EventType::ButtonReleased(btn, _) => {
                        if let Some(gamepad_index) = self.get_gamepad_index(eve.id) {
                            _ = self.dispatch.send(
                                GamepadButtonEvent {
                                    button: Self::gilrs_to_button_code(btn),
                                    gamepad_index,
                                    state: GamepadButtonState::Released,
                                }
                                .into_message(),
                            )
                        }
                    }
                    EventType::AxisChanged(axis, degree, _) => {
                        if let Some((axis, x, y)) = match axis {
                            gilrs::Axis::LeftStickX => {
                                Some((GamepadAxis::StickLeft, Some(degree), None))
                            }
                            gilrs::Axis::LeftStickY => {
                                Some((GamepadAxis::StickLeft, None, Some(degree)))
                            }
                            gilrs::Axis::RightStickX => {
                                Some((GamepadAxis::StickRight, Some(degree), None))
                            }
                            gilrs::Axis::RightStickY => {
                                Some((GamepadAxis::StickRight, None, Some(degree)))
                            }
                            gilrs::Axis::LeftZ => Some((GamepadAxis::Z, Some(degree), None)),
                            gilrs::Axis::RightZ => Some((GamepadAxis::Z, None, Some(degree))),
                            gilrs::Axis::DPadX => Some((GamepadAxis::Dpad, Some(degree), None)),
                            gilrs::Axis::DPadY => Some((GamepadAxis::Dpad, None, Some(degree))),
                            gilrs::Axis::Unknown => None,
                        } {
                            if let Some((gamepad_index, (x, y))) =
                                self.apply_axis(eve.id, axis, x, y)
                            {
                                _ = self.dispatch.send(
                                    GamepadAxisMoved {
                                        gamepad_index,
                                        axis,
                                        x,
                                        y,
                                    }
                                    .into_message(),
                                );
                            }
                        }
                    }
                    EventType::Connected => {
                        let gamepad_index = if let Some(index) = self.recycled_gamepad_indexes.pop()
                        {
                            index
                        } else {
                            let index = self.next_gamepad_index;
                            self.next_gamepad_index = self.next_gamepad_index + 1;
                            index
                        };

                        tracing::info!("Gilrs gamepad connected: {} ({})", gamepad_index, eve.id);
                        _ = self.connected_gamepads.insert(
                            eve.id,
                            GamepadInfo {
                                axis: HashMap::new(),
                                gamepad_index,
                            },
                        );
                        _ = self
                            .dispatch
                            .send(GamepadConnected { gamepad_index }.into_message());
                    }
                    EventType::Disconnected => {
                        if let Some(gamepad_index) = self.get_gamepad_index(eve.id) {
                            tracing::info!(
                                "Gilrs gamepad disconnected: {} ({})",
                                gamepad_index,
                                eve.id
                            );

                            self.recycled_gamepad_indexes.push(gamepad_index);
                            _ = self.connected_gamepads.remove(&eve.id);

                            _ = self
                                .dispatch
                                .send(GamepadDisonnected { gamepad_index }.into_message());
                        } else {
                            tracing::warn!("Gilrs unrecognized gamepad disconnected ({})", eve.id);
                        }
                    }
                    EventType::Dropped => {}
                }
            }
        }
        ExecutionResult::Processed
    }
}
impl IsDisposable for GilrsActor {
    fn dispose(&mut self) {}
}
impl IsActor for GilrsActor {
    fn weight(&self) -> u32 {
        0
    }

    fn name(&self) -> String {
        "gilrs".to_string()
    }
}

pub struct GilrsPlugin {
    dispatcher: Sender<Message>,
}
impl GilrsPlugin {
    pub fn new(dispatcher: Sender<Message>) -> Self {
        Self { dispatcher }
    }
}
impl ActorsPlugin for GilrsPlugin {
    fn apply<T: waef_core::actors::UseActors>(self, target: T) -> T {
        target.use_actor(Box::new(GilrsActor::new(self.dispatcher)))
    }
}
