use waef_core::{
    core::{IsMessage, Message},
    util::pod::Pod,
};

#[repr(C)]
#[derive(Clone, Copy)]
pub struct GilrsRebindGamepad {
    pub target_index: u32,
    pub current_index: u32,
}
unsafe impl Pod for GilrsRebindGamepad {}

impl IsMessage for GilrsRebindGamepad {
    fn into_message(self) -> Message {
        Message::new_from_pod(Self::category_name(), self)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Self::try_from_bytes(&msg.buffer)
    }

    fn category_name() -> &'static str {
        "gilrs:rebind_gamepad"
    }
}
